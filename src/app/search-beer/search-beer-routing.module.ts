import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchBeerComponent } from 'src/app/search-beer/search-beer.component';

const searchRoutes: Routes = [
  { path: '', component: SearchBeerComponent }
];

@NgModule({
  imports: [RouterModule.forChild(searchRoutes)],
  exports: [RouterModule]
})
export class SearchBeerRoutingModule { }
