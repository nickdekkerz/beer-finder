import { animate, AnimationMetadata, state, style, transition, trigger } from '@angular/animations';

export const fadeInAndOut: AnimationMetadata[] = [
  trigger('fadeInAndOut', [
    state('fadedIn', style({
      opacity: 1,
    })),
    state('fadedOut', style({
      opacity: 0,
    })),
    transition('fadedOut => fadedIn', [
      animate('1s ease-out')
    ]),
    transition('fadedIn => fadedOut', [
      animate('0.2s')
    ])
  ])
];
