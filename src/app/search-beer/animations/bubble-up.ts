import { animate, AnimationTriggerMetadata, state, style, transition, trigger } from '@angular/animations';

export const bubbleUpAnimations: AnimationTriggerMetadata[] = [
  trigger('bubbleUpStyles', [
    state('shown', style({
      left: '5%',
      opacity: 1,
      top: '5%'
    })),
    state('hidden', style({
      left: '5%',
      opacity: 0,
      top: '100%'
    })),
    transition('hidden => shown', [
      animate('2s 1s ease-out')
    ])
  ]),

  trigger('bubbleUpBrewery', [
    state('shown', style({
      opacity: 1,
      right: '40%',
      top: 0
    })),
    state('hidden', style({
      opacity: 0,
      right: '40%',
      top: '100%'
    })),
    transition('hidden => shown', [
      animate('1.5s 0.75s ease-out')
    ])
  ]),

  trigger('bubbleUpAlcoholPercentage', [
    state('shown', style({
      bottom: 0,
      left: '20%',
      opacity: 1
    })),
    state('hidden', style({
      bottom: '-10%',
      left: '20%',
      opacity: 0
    })),
    transition('hidden => shown', [
      animate('1s 1.5s ease-out')
    ])
  ]),

  trigger('bubbleUpSearchButton', [
    state('shown', style({
      bottom: '20%',
      opacity: 1,
      right: '35%'
    })),
    state('hidden', style({
      bottom: '-10%',
      opacity: 0,
      right: '35%'
    })),
    transition('hidden <=> shown', [
      animate('0.5s')
    ])
  ])
];
