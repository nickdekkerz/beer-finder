import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { Beer, Beers } from 'src/app/core/beer/beer.model';
import { BeerStoreService } from 'src/app/core/beer/beer.store.service';
import { bubbleUpAnimations, fadeInAndOut } from 'src/app/search-beer/animations';
import { DecimalToNumberPipe } from 'src/app/search-beer/pipes/decimal-to-number.pipe';
import { PercentageToNumberPipe } from 'src/app/search-beer/pipes/percentage-to-number.pipe';

@Component({
  selector: 'app-search',
  templateUrl: './search-beer.component.html',
  styleUrls: ['./search-beer.component.scss'],
  animations: [bubbleUpAnimations, fadeInAndOut]
})
export class SearchBeerComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;
  public beers: Beers;
  public styles: Array<string>;
  public breweries: Array<string>;
  public showBubbles = false;
  public matchingBeer: Beer;

  private readonly percentageRegex: RegExp = new RegExp('^((100([,.]0{1,2})?)|(0|[1-9][0-9]?)([,.][0-9]{1,2})?)$');
  private subscriptions: Subscription = new Subscription();

  public onFormChange(): void {
    if (this.matchingBeer) {
      this.matchingBeer = undefined;
    }
  }

  public get formHasValues(): boolean {
    return this.form.value.style || this.form.value.brewery || this.form.value.alcoholPercentage;
  }

  public get style(): AbstractControl {
    return this.form.get('style');
  }

  public get brewery(): AbstractControl {
    return this.form.get('brewery');
  }

  public get alcoholPercentage(): AbstractControl {
    return this.form.get('alcoholPercentage');
  }

  public constructor(private beerStoreService: BeerStoreService,
                     private formBuilder: FormBuilder) { }

  public ngOnInit(): void {
    this.createForm();
    this.setBeersSubscription();
  }

  public ngAfterViewInit(): void {
    setTimeout(() => {
      this.showBubbles = true;
    });
  }

  public ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public onStyleChange(): void {
    this.updateBreweryList();
  }

  public onBreweryChange(): void {
    this.updateStyleList();
  }

  public search(): void {
    const selectedStyle: string = this.form.value.style;
    const selectedBrewery: string = this.form.value.brewery;
    const selectedAlcoholPercentage: number = new DecimalToNumberPipe().transform(this.form.value.alcoholPercentage);

    const beersFilteredOnOptions: Beers = this.beers.filter(
      (beer: Beer) => beer.Stijl === selectedStyle && beer.Brouwerij === selectedBrewery
    );

    const beersFilteredOnAlcoholPercentage: Beers = beersFilteredOnOptions.filter(
      (beer: Beer) => new PercentageToNumberPipe().transform(beer.Alcoholpercentage) >= selectedAlcoholPercentage
    );

    if (beersFilteredOnAlcoholPercentage.length > 0) {
      this.matchingBeer = beersFilteredOnAlcoholPercentage[0];
    } else {
      this.matchingBeer = beersFilteredOnOptions[0];
    }
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      style: ['', Validators.required],
      brewery: ['', Validators.required],
      alcoholPercentage: ['', Validators.pattern(this.percentageRegex)]
    });

    const formStatusChangeSubscription: Subscription = this.form.statusChanges
      .pipe(distinctUntilChanged())
      .subscribe({
        next: (status: string) => this.handleFormStatus(status)
      });

    this.subscriptions.add(formStatusChangeSubscription);
  }

  private handleFormStatus(status: string): void {
    if (status === 'INVALID') {
      this.matchingBeer = undefined;
    }
  }

  private setBeersSubscription(): void {
    const beersSubscription: Subscription = this.beerStoreService.beers$.subscribe({
      next: (beers: Beers) => {
        this.extractBeerProperties(beers);
        this.beers = beers;
      }
    });

    this.subscriptions.add(beersSubscription);
  }

  private extractBeerProperties(beers: Beers): void {
    this.styles = this.extractBeerProperty<string>(beers, this.getStyle);
    this.breweries = this.extractBeerProperty<string>(beers, this.getBrewery);
  }

  private extractBeerProperty<T>(beers: Beers, propertyRetrievalCallback: (beer: Beer) => T): Array<T> {
    return [...new Set(
      beers
        .map(propertyRetrievalCallback)
        .sort()
        .filter(this.nonEmptyValue)
    )];
  }

  private updateStyleList(): void {
    this.styles = this.extractBeerProperty<string>(
      this.beers.filter((beer: Beer) => this.breweryMatches(beer)),
      this.getStyle
    );
  }

  private updateBreweryList(): void {
    this.breweries = this.extractBeerProperty<string>(
      this.beers.filter((beer: Beer) => this.styleMatches(beer)),
      this.getBrewery
    );
  }

  private styleMatches(beer: Beer): boolean {
    return !(this.style.value && beer.Stijl !== this.style.value);
  }

  private breweryMatches(beer: Beer): boolean {
    return !(this.brewery.value && beer.Brouwerij !== this.brewery.value);
  }

  private getBrewery = (beer: Beer): string => beer.Brouwerij;

  private getStyle = (beer: Beer): string => beer.Stijl;

  private nonEmptyValue = (value: any): boolean => value;
}
