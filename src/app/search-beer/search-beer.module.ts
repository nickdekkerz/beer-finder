import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchBeerRoutingModule } from 'src/app/search-beer/search-beer-routing.module';
import { SearchBeerComponent } from 'src/app/search-beer/search-beer.component';
import { PercentageToNumberPipe } from './pipes/percentage-to-number.pipe';
import { DecimalToNumberPipe } from './pipes/decimal-to-number.pipe';


@NgModule({
  declarations: [SearchBeerComponent, PercentageToNumberPipe, DecimalToNumberPipe],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SearchBeerRoutingModule,
  ]
})
export class SearchBeerModule { }
