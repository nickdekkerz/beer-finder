import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { Beers } from 'src/app/core/beer/beer.model';
import { BeerStoreService } from 'src/app/core/beer/beer.store.service';
import { SearchBeerComponent } from 'src/app/search-beer/search-beer.component';

describe('SearchComponent', () => {
  let searchBeerComponent: SearchBeerComponent;
  let fixture: ComponentFixture<SearchBeerComponent>;
  let beerStoreServiceSpy: jasmine.SpyObj<BeerStoreService>;

  const dummyBeers: Beers = [
    {
      id: 1, Bier: '1410', Stijl: 'speciaalbier', Stamwortgehalte: '13.0',
      Alcoholpercentage: '5,5%', Gisting: 'hoog', Sinds: 2010, Brouwerij: 'SNAB'
    },
    {
      id: 2, Bier: 'Alfa Edel Pils', Stijl: 'pilsener', Stamwortgehalte: '12.0',
      Alcoholpercentage: '5,0%', Gisting: 'laag', Sinds: null, Brouwerij: 'Alfa'
    },
    {
      id: 3, Bier: 'Alfa Bokbier', Stijl: 'herfstbok', Stamwortgehalte: '16.3',
      Alcoholpercentage: '6,5%', Gisting: 'laag', Sinds: 1981, Brouwerij: 'Alfa'
    },
    {
      id: 4, Bier: 'Alfa Oud Bruin', Stijl: 'oud bruin', Stamwortgehalte: '',
      Alcoholpercentage: '2,5%', Gisting: 'laag', Sinds: null, Brouwerij: 'Alfa'
    },
  ];

  beforeEach(async(() => {
    const beerStoreServiceMock = jasmine.createSpyObj('BeerStoreService', ['beers$']);

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [SearchBeerComponent],
      providers: [
        FormBuilder,
        { provide: BeerStoreService, useValue: beerStoreServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchBeerComponent);
    searchBeerComponent = fixture.componentInstance;
    beerStoreServiceSpy = TestBed.get(BeerStoreService);
    beerStoreServiceSpy.beers$ = of(dummyBeers);
  }));

  it('should create', () => {
    expect(searchBeerComponent).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should extract and fill the data on beers', () => {
      searchBeerComponent.ngOnInit();
      expect(searchBeerComponent.beers).toEqual(dummyBeers, 'beers');
      expect(searchBeerComponent.styles).toEqual(jasmine.arrayContaining(['oud bruin', 'herfstbok', 'pilsener', 'speciaalbier']), 'styles');
      expect(searchBeerComponent.styles.length).toEqual(4, 'styles length');
      expect(searchBeerComponent.breweries).toEqual(jasmine.arrayContaining(['Alfa', 'SNAB']), 'breweries');
      expect(searchBeerComponent.breweries.length).toEqual(2, 'breweries length');
    });

    it('should reset the matchingBeer when the form becomes invalid', fakeAsync(() => {
      searchBeerComponent.ngOnInit();
      searchBeerComponent.form.controls.style.setValue('style');
      searchBeerComponent.form.controls.brewery.setValue('brewery');
      searchBeerComponent.matchingBeer = dummyBeers[0];
      searchBeerComponent.form.controls.style.setValue('');
      expect(searchBeerComponent.matchingBeer).toBeUndefined();
    }));
  });

  describe('ngAfterViewInit', () => {
    it('should set the showBubbles property to true', fakeAsync(() => {
      searchBeerComponent.showBubbles = false;
      searchBeerComponent.ngAfterViewInit();
      tick(1);
      expect(searchBeerComponent.showBubbles).toBe(true);
    }));
  });

  describe('onFormChange', () => {
    it('should reset the matchingBeer if it is defined', () => {
      searchBeerComponent.matchingBeer = dummyBeers[0];
      searchBeerComponent.onFormChange();
      expect(searchBeerComponent.matchingBeer).toBeUndefined();
    });
  });

  describe('formHasValue', () => {
    it('should be truthy when all form values are set', () => {
      searchBeerComponent.form = createNewForm('style', 'brewery', 'alcoholPercentage');
      expect(searchBeerComponent.formHasValues).toBeTruthy();
    });

    it('should be truthy when the style and brewery value are set', () => {
      searchBeerComponent.form = createNewForm('style', 'brewery', '');
      expect(searchBeerComponent.formHasValues).toBeTruthy();
    });

    it('should be truthy when the style and alcohol percentage value are set', () => {
      searchBeerComponent.form = createNewForm('style', '', 'alcoholPercentage');
      expect(searchBeerComponent.formHasValues).toBeTruthy();
    });

    it('should be truthy when the brewery and alcohol percentage value are set', () => {
      searchBeerComponent.form = createNewForm('', 'brewery', 'alcoholPercentage');
      expect(searchBeerComponent.formHasValues).toBeTruthy();
    });


    it('should be truthy when only the style value is set', () => {
      searchBeerComponent.form = createNewForm('style', '', '');
      expect(searchBeerComponent.formHasValues).toBeTruthy();
    });

    it('should be truthy when only the brewery value is set', () => {
      searchBeerComponent.form = createNewForm('', 'brewery', '');
      expect(searchBeerComponent.formHasValues).toBeTruthy();
    });

    it('should be truthy when only the alcohol percentage value is set', () => {
      searchBeerComponent.form = createNewForm('', '', 'alcoholPercentage');
      expect(searchBeerComponent.formHasValues).toBeTruthy();
    });

    it('should be falsy when none of the form values are set', () => {
      searchBeerComponent.form = createNewForm('', '', '');
      expect(searchBeerComponent.formHasValues).toBeFalsy();
    });
  });

  describe('onStyleChange', () => {
    it('should update the brewery selection list when a style is changed', () => {
      // First fill the options lists with the dummy data
      searchBeerComponent.ngOnInit();
      expect(searchBeerComponent.breweries).toEqual(jasmine.arrayContaining(['Alfa', 'SNAB']), 'breweries before');
      expect(searchBeerComponent.breweries.length).toEqual(2, 'breweries length before');

      searchBeerComponent.form.controls.style.setValue('pilsener');
      searchBeerComponent.onStyleChange();
      expect(searchBeerComponent.breweries).toEqual(jasmine.arrayContaining(['Alfa']), 'breweries after');
      expect(searchBeerComponent.breweries.length).toEqual(1, 'breweries length after');
    });
  });

  describe('onBreweryChange', () => {
    it('should update the style selection list when a brewery is changed', () => {
      // First fill the options lists with the dummy data
      searchBeerComponent.ngOnInit();
      expect(searchBeerComponent.styles).toEqual(
        jasmine.arrayContaining(['oud bruin', 'herfstbok', 'pilsener', 'speciaalbier']),
        'styles before'
      );
      expect(searchBeerComponent.styles.length).toEqual(4, 'styles length, before');

      searchBeerComponent.form.controls.brewery.setValue('Alfa');
      searchBeerComponent.onBreweryChange();
      expect(searchBeerComponent.styles).toEqual(
        jasmine.arrayContaining(['oud bruin', 'herfstbok', 'pilsener']),
        'styles after'
      );
      expect(searchBeerComponent.styles.length).toEqual(3, 'styles length after');
    });
  });
});

function createNewForm(styleValue: string, breweryValue: string, alcoholPercentage: string): FormGroup {
  return new FormGroup({
    style: new FormControl(styleValue),
    brewery: new FormControl(breweryValue),
    alcoholPercentage: new FormControl(alcoholPercentage)
  });
}
