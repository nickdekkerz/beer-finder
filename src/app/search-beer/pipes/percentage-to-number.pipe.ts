import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percentageToNumber'
})
export class PercentageToNumberPipe implements PipeTransform {
  public transform(value: string): number {
    if (!value) {
      return 0;
    }

    return parseFloat(value.replace(',', '.').replace('%', ''));
  }
}
