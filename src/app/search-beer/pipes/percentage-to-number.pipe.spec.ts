import { PercentageToNumberPipe } from './percentage-to-number.pipe';

describe('PercentageToNumberPipe', () => {
  it('create an instance', () => {
    const pipe = new PercentageToNumberPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return 0 when value is undefined', () => {
    expect(new PercentageToNumberPipe().transform(undefined)).toEqual(0);
  });

  it('should return the corresponding number when value is defined', () => {
    expectDecimalToReturnNumber('0.00%', 0);
    expectDecimalToReturnNumber('0.01%', 0.01);
    expectDecimalToReturnNumber('1.23%', 1.23);
    expectDecimalToReturnNumber('12%', 12);
    expectDecimalToReturnNumber('12.23%', 12.23);
    expectDecimalToReturnNumber('100.00%', 100);
    expectDecimalToReturnNumber('90,00%', 90);
  });
});

function expectDecimalToReturnNumber(value: string, output: number): void {
  expect(new PercentageToNumberPipe().transform(value)).toEqual(output);
}

