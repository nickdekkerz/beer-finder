import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimalToNumber'
})
export class DecimalToNumberPipe implements PipeTransform {
  public transform(value: string): number {
    if (!value) {
      return 0;
    }

    return parseFloat(value.replace(',', '.'));
  }

}
