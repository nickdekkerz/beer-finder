import { Component, OnInit } from '@angular/core';
import { BeerStoreService } from 'src/app/core/beer/beer.store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public constructor(private beerStoreService: BeerStoreService) { }

  public ngOnInit(): void {
    this.beerStoreService.getBeers();
  }
}
