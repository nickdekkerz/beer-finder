import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from 'src/app/app.component';
import { BeerStoreService } from 'src/app/core/beer/beer.store.service';

describe('AppComponent', () => {
  let appComponent: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let beerStoreServiceSpy: jasmine.SpyObj<BeerStoreService>;

  beforeEach(async(() => {
    const beerStoreServiceMock = jasmine.createSpyObj('BeerStoreService', ['getBeers']);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [AppComponent],
      providers: [
        { provide: BeerStoreService, useValue: beerStoreServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    appComponent = fixture.componentInstance;
    beerStoreServiceSpy = TestBed.get(BeerStoreService);
  }));

  it('should create the app', () => {
    expect(appComponent).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should call the BeerStoreService to get the beers', () => {
      appComponent.ngOnInit();
      expect(beerStoreServiceSpy.getBeers).toHaveBeenCalledTimes(1);
    });
  });
});
