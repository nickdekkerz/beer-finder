import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Beers, BeersModel } from 'src/app/core/beer/beer.model';
import { BeerService } from 'src/app/core/beer/beer.service';
import { BeerStoreService } from 'src/app/core/beer/beer.store.service';


describe('BeerStoreService', () => {
  let beerStoreService: BeerStoreService;
  let beerServiceSpy: jasmine.SpyObj<BeerService>;

  const beersModel: BeersModel = require('../../../assets/beers.json');

  beforeEach(() => {
    const beerServiceMock = jasmine.createSpyObj('BeerService', ['getBeers']);

    TestBed.configureTestingModule({
      providers: [
        BeerStoreService,
        { provide: BeerService, useValue: beerServiceMock }
      ]
    });

    beerStoreService = TestBed.get(BeerStoreService);
    beerServiceSpy = TestBed.get(BeerService);
  });

  it('should be created', () => {
    expect(beerStoreService).toBeTruthy();
  });

  describe('getBeers', () => {
    beforeEach(() => {
      beerServiceSpy.getBeers.and.returnValue(of(beersModel));
    });

    it('should call the beerService to get the beers data', () => {
      beerStoreService.getBeers();
      expect(beerServiceSpy.getBeers).toHaveBeenCalledTimes(1);
    });

    it('should next the beers data onto the stream', () => {
      beerStoreService.beers$.subscribe({
        next: (beers: Beers) => expect(beers).toEqual(beersModel.bieren)
      });
      beerStoreService.getBeers();
    });
  });
});
