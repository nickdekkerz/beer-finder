export interface Beer {
  id: number;
  Bier: string;
  Stijl: string;
  Stamwortgehalte: string;
  Alcoholpercentage: string;
  Gisting: string;
  Sinds: number;
  Brouwerij: string;
}

export interface BeersModel {
  bieren: Beers;
}

export type Beers = Array<Beer>;
