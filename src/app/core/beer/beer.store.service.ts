import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Beers, BeersModel } from './beer.model';
import { BeerService } from './beer.service';

@Injectable({
  providedIn: 'root'
})
export class BeerStoreService {
  public beers$: Observable<Beers>;
  private beersSubject: BehaviorSubject<Beers> = new BehaviorSubject<Beers>([]);

  public constructor(private beerService: BeerService) {
    this.beers$ = this.beersSubject.asObservable()
      .pipe(filter((beers: Beers) => beers.length > 0));
  }

  public getBeers(): void {
    this.beerService.getBeers().subscribe({
      next: (beersModel: BeersModel) => this.beersSubject.next(beersModel.bieren)
    });
  }
}
