import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { BeersModel } from 'src/app/core/beer/beer.model';
import { BeerService } from 'src/app/core/beer/beer.service';

describe('BeerService', () => {
  let beerService: BeerService;
  let httpClientSpy: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BeerService]
    });

    beerService = TestBed.get(BeerService);
    httpClientSpy = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpClientSpy.verify();
  });

  it('should be created', () => {
    expect(beerService).toBeTruthy();
  });

  describe('getBeers', () => {
    it('should return beers data', () => {
      const dummyBeersModel = {
        bieren: [
          {
            id: 1, Bier: '1410', Stijl: 'speciaalbier', Stamwortgehalte: '13.0',
            Alcoholpercentage: '5,5%', Gisting: 'hoog', Sinds: 2010, Brouwerij: 'SNAB'
          },
          {
            id: 2, Bier: 'Alfa Edel Pils', Stijl: 'pilsener', Stamwortgehalte: '12.0',
            Alcoholpercentage: '5,0%', Gisting: 'laag', Sinds: null, Brouwerij: 'Alfa'
          }
        ]
      };

      beerService.getBeers().subscribe((beersModel: BeersModel) => {
        expect(beersModel).toEqual(dummyBeersModel);
      });

      const req = httpClientSpy.expectOne('/assets/beers.json');
      expect(req.request.method).toBe('GET');
      req.flush(dummyBeersModel);
    });
  });
});
