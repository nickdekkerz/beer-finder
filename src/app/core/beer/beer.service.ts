import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BeersModel } from 'src/app/core/beer/beer.model';


@Injectable({
  providedIn: 'root'
})
export class BeerService {
  public constructor(private httpClient: HttpClient) { }

  public getBeers(): Observable<BeersModel> {
    return this.httpClient.get<BeersModel>('/assets/beers.json');
  }
}
