# BeerFinder
The beer finder is an app that finds the perfect beer to give as a birthday present.

## Development
### Prerequisites
Make sure the [Angular CLI](https://cli.angular.io/) is installed.

### Running the code
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Testing
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


